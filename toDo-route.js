const { randomUUID } = require('crypto')
var express = require('express')
var mongoose= require('mongoose')
var router = express.Router()
var  Todo = require('./todo-Schema')
// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})
// get only completed todo
router.post('/completedTodos', function (req, res) {
    var method = req.params.method;
    var termToFilterBy = req.body.term;

    Todo.find({method: termToFilterBy}, function(err, products) {
        res.send(products);
    });

})
// define the about route
router.post('/post',async (req, res)=>{
    if(!req.body) {
        return res.status(400).send({
            message: "Todo  content can not be empty"
        });
    }

    // Create a Note
    const note = new Todo({
        title: req.body.title || "Untitled Note",
        isCompleted: req.body.isCompleted
    });

    // Save Note in the database
    note.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });


})
// define the home page route
router.get('/', function (req, res) {
    Todo.find()
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
})
// define the about route
router.post('/post',async (req, res)=>{
    if(!req.body) {
        return res.status(400).send({
            message: "Todo  content can not be empty"
        });
    }

    // Create a Note
    const note = new Todo({
        title: req.body.title || "Untitled Note",
        isCompleted: req.body.isCompleted
    });

    // Save Note in the database
    note.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });


})


// get one to do
router.get('/:toDoId',(req,res)=>{
    Todo.findById(req.params.toDoId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.toDoId
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.toDoId
            });
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.toDoId
        });
    });
})


// update todo  datdad

router.put('/:toDoId', (req,res)=>{

    if(!req.body) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    Todo.findByIdAndUpdate(req.params.toDoId, {
        title: req.body.title || "Untitled Note",
        isCompleted: req.body.isCompleted
    }, {new: true})
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.toDoId
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.toDoId
            });
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.toDoId
        });
    });
})

//set  completed or not

router.put("/complete/:toDoId",(req,res)=>{
    Todo.findByIdAndUpdate(req.params.toDoId, {

        isCompleted: req.body.isCompleted
    }, {new: true})
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.toDoId
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.toDoId
            });
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.toDoId
        });
    });
})





router.delete("/:toDoId", (req,res)=>{
    Todo.findByIdAndRemove(req.params.toDoId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.toDoId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.toDoId
            });
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.toDoId
        });
    });
})

module.exports = router