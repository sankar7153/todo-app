const mongoose = require('mongoose');


const toDoSchema = mongoose.Schema({
  title: { type: String, required: true },
  isCompleted: { type: Boolean, required: true },

});

module.exports = mongoose.model('TODO', toDoSchema);