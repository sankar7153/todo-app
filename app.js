
var express = require('express');

var mongoose = require('mongoose')
var app = express();
const cors = require("cors");
app.use(cors());

app.use(express.json());


var toDo = require('./toDo-route');
app.use("/toDo", toDo)


mongoose.connect('mongodb://localhost:27017/TodoDB',
  {
    useNewUrlParser: true,

    useUnifiedTopology: true
  }
);
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error 1: "));
db.once("open", function () {
  console.log("Connected successfully");
});

app.listen(3000)